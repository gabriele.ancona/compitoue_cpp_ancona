// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPITOUNREAL_TPS_BTTask_ClearBlackboardValue_generated_h
#error "BTTask_ClearBlackboardValue.generated.h already included, missing '#pragma once' in BTTask_ClearBlackboardValue.h"
#endif
#define COMPITOUNREAL_TPS_BTTask_ClearBlackboardValue_generated_h

#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_SPARSE_DATA
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_RPC_WRAPPERS
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTask_ClearBlackboardValue(); \
	friend struct Z_Construct_UClass_UBTTask_ClearBlackboardValue_Statics; \
public: \
	DECLARE_CLASS(UBTTask_ClearBlackboardValue, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CompitoUnreal_TPS"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_ClearBlackboardValue)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTTask_ClearBlackboardValue(); \
	friend struct Z_Construct_UClass_UBTTask_ClearBlackboardValue_Statics; \
public: \
	DECLARE_CLASS(UBTTask_ClearBlackboardValue, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CompitoUnreal_TPS"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_ClearBlackboardValue)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_ClearBlackboardValue(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_ClearBlackboardValue) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_ClearBlackboardValue); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_ClearBlackboardValue); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_ClearBlackboardValue(UBTTask_ClearBlackboardValue&&); \
	NO_API UBTTask_ClearBlackboardValue(const UBTTask_ClearBlackboardValue&); \
public:


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_ClearBlackboardValue(UBTTask_ClearBlackboardValue&&); \
	NO_API UBTTask_ClearBlackboardValue(const UBTTask_ClearBlackboardValue&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_ClearBlackboardValue); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_ClearBlackboardValue); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBTTask_ClearBlackboardValue)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_PRIVATE_PROPERTY_OFFSET
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_12_PROLOG
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_PRIVATE_PROPERTY_OFFSET \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_SPARSE_DATA \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_RPC_WRAPPERS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_INCLASS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_PRIVATE_PROPERTY_OFFSET \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_SPARSE_DATA \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_INCLASS_NO_PURE_DECLS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPITOUNREAL_TPS_API UClass* StaticClass<class UBTTask_ClearBlackboardValue>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTTask_ClearBlackboardValue_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

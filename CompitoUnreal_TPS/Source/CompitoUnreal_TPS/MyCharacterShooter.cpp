// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacterShooter.h"
#include "Gun.h"
#include "Components/CapsuleComponent.h"
#include "CompitoUnreal_TPSGameModeBase.h"

// Sets default values
AMyCharacterShooter::AMyCharacterShooter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyCharacterShooter::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	Gun = GetWorld()->SpawnActor<AGun>(GunClass);
	GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
	Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
	Gun->SetOwner(this);
	
}

// Called every frame
void AMyCharacterShooter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyCharacterShooter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//gestione input 
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMyCharacterShooter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMyCharacterShooter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	//for joystick
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AMyCharacterShooter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AMyCharacterShooter::LookRightRate);

	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Shoot"), EInputEvent::IE_Pressed, this, &AMyCharacterShooter::Shoot);

}

void AMyCharacterShooter::MoveForward(float AxisValue) 
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AMyCharacterShooter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AMyCharacterShooter::LookUpRate(float AxisValue)
{
	AddControllerPitchInput(AxisValue * rotationRate * GetWorld()->GetDeltaSeconds());
}

void AMyCharacterShooter::LookRightRate(float AxisValue)
{
	AddControllerYawInput(AxisValue * rotationRate * GetWorld()->GetDeltaSeconds());
}

void AMyCharacterShooter::Shoot()
{
	Gun->PullTrigger();
}

float AMyCharacterShooter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	DamageToApply = FMath::Min(Health, DamageToApply);
	Health -= DamageToApply;
	UE_LOG(LogTemp, Warning, TEXT("Health left %f"), Health);

	if(IsDead())
	{
		ACompitoUnreal_TPSGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ACompitoUnreal_TPSGameModeBase>();
		if (GameMode != nullptr)
		{
			GameMode->PawnKilled(this);
		}

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);		
	}

	return DamageToApply;
}

bool  AMyCharacterShooter::IsDead() const
{
	return Health <= 0;
}

float AMyCharacterShooter::GetHealthPercent() const
{
	return Health / MaxHealth;
}


// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPITOUNREAL_TPS_MyCharacterShooter_generated_h
#error "MyCharacterShooter.generated.h already included, missing '#pragma once' in MyCharacterShooter.h"
#endif
#define COMPITOUNREAL_TPS_MyCharacterShooter_generated_h

#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_SPARSE_DATA
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetHealthPercent); \
	DECLARE_FUNCTION(execIsDead);


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetHealthPercent); \
	DECLARE_FUNCTION(execIsDead);


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyCharacterShooter(); \
	friend struct Z_Construct_UClass_AMyCharacterShooter_Statics; \
public: \
	DECLARE_CLASS(AMyCharacterShooter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CompitoUnreal_TPS"), NO_API) \
	DECLARE_SERIALIZER(AMyCharacterShooter)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMyCharacterShooter(); \
	friend struct Z_Construct_UClass_AMyCharacterShooter_Statics; \
public: \
	DECLARE_CLASS(AMyCharacterShooter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CompitoUnreal_TPS"), NO_API) \
	DECLARE_SERIALIZER(AMyCharacterShooter)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyCharacterShooter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyCharacterShooter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacterShooter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacterShooter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCharacterShooter(AMyCharacterShooter&&); \
	NO_API AMyCharacterShooter(const AMyCharacterShooter&); \
public:


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyCharacterShooter(AMyCharacterShooter&&); \
	NO_API AMyCharacterShooter(const AMyCharacterShooter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyCharacterShooter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyCharacterShooter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyCharacterShooter)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__rotationRate() { return STRUCT_OFFSET(AMyCharacterShooter, rotationRate); } \
	FORCEINLINE static uint32 __PPO__MaxHealth() { return STRUCT_OFFSET(AMyCharacterShooter, MaxHealth); } \
	FORCEINLINE static uint32 __PPO__Health() { return STRUCT_OFFSET(AMyCharacterShooter, Health); } \
	FORCEINLINE static uint32 __PPO__GunClass() { return STRUCT_OFFSET(AMyCharacterShooter, GunClass); } \
	FORCEINLINE static uint32 __PPO__Gun() { return STRUCT_OFFSET(AMyCharacterShooter, Gun); }


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_11_PROLOG
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_PRIVATE_PROPERTY_OFFSET \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_SPARSE_DATA \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_RPC_WRAPPERS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_INCLASS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_PRIVATE_PROPERTY_OFFSET \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_SPARSE_DATA \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_INCLASS_NO_PURE_DECLS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPITOUNREAL_TPS_API UClass* StaticClass<class AMyCharacterShooter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CompitoUnreal_TPS_Source_CompitoUnreal_TPS_MyCharacterShooter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPITOUNREAL_TPS_BTService_PlayerLocationIfSeen_generated_h
#error "BTService_PlayerLocationIfSeen.generated.h already included, missing '#pragma once' in BTService_PlayerLocationIfSeen.h"
#endif
#define COMPITOUNREAL_TPS_BTService_PlayerLocationIfSeen_generated_h

#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_SPARSE_DATA
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_RPC_WRAPPERS
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTService_PlayerLocationIfSeen(); \
	friend struct Z_Construct_UClass_UBTService_PlayerLocationIfSeen_Statics; \
public: \
	DECLARE_CLASS(UBTService_PlayerLocationIfSeen, UBTService_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CompitoUnreal_TPS"), NO_API) \
	DECLARE_SERIALIZER(UBTService_PlayerLocationIfSeen)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBTService_PlayerLocationIfSeen(); \
	friend struct Z_Construct_UClass_UBTService_PlayerLocationIfSeen_Statics; \
public: \
	DECLARE_CLASS(UBTService_PlayerLocationIfSeen, UBTService_BlackboardBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CompitoUnreal_TPS"), NO_API) \
	DECLARE_SERIALIZER(UBTService_PlayerLocationIfSeen)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTService_PlayerLocationIfSeen(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTService_PlayerLocationIfSeen) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTService_PlayerLocationIfSeen); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTService_PlayerLocationIfSeen); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTService_PlayerLocationIfSeen(UBTService_PlayerLocationIfSeen&&); \
	NO_API UBTService_PlayerLocationIfSeen(const UBTService_PlayerLocationIfSeen&); \
public:


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTService_PlayerLocationIfSeen(UBTService_PlayerLocationIfSeen&&); \
	NO_API UBTService_PlayerLocationIfSeen(const UBTService_PlayerLocationIfSeen&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTService_PlayerLocationIfSeen); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTService_PlayerLocationIfSeen); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBTService_PlayerLocationIfSeen)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_PRIVATE_PROPERTY_OFFSET
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_12_PROLOG
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_PRIVATE_PROPERTY_OFFSET \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_SPARSE_DATA \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_RPC_WRAPPERS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_INCLASS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_PRIVATE_PROPERTY_OFFSET \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_SPARSE_DATA \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_INCLASS_NO_PURE_DECLS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPITOUNREAL_TPS_API UClass* StaticClass<class UBTService_PlayerLocationIfSeen>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CompitoUnreal_TPS_Source_CompitoUnreal_TPS_BTService_PlayerLocationIfSeen_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

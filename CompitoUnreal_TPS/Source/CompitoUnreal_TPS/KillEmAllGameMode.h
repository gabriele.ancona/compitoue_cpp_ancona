// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CompitoUnreal_TPSGameModeBase.h"
#include "KillEmAllGameMode.generated.h"

/**
 * 
 */
UCLASS()
class COMPITOUNREAL_TPS_API AKillEmAllGameMode : public ACompitoUnreal_TPSGameModeBase
{
	GENERATED_BODY()

public:
	virtual void PawnKilled(APawn* Pawnkilled) override;

private:
	void EndGame(bool bIsPlayerWinner);
	
};

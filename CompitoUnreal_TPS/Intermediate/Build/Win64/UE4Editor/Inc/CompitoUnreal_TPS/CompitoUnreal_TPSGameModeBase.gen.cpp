// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CompitoUnreal_TPS/CompitoUnreal_TPSGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompitoUnreal_TPSGameModeBase() {}
// Cross Module References
	COMPITOUNREAL_TPS_API UClass* Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_NoRegister();
	COMPITOUNREAL_TPS_API UClass* Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_CompitoUnreal_TPS();
// End Cross Module References
	void ACompitoUnreal_TPSGameModeBase::StaticRegisterNativesACompitoUnreal_TPSGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_NoRegister()
	{
		return ACompitoUnreal_TPSGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CompitoUnreal_TPS,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "CompitoUnreal_TPSGameModeBase.h" },
		{ "ModuleRelativePath", "CompitoUnreal_TPSGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACompitoUnreal_TPSGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics::ClassParams = {
		&ACompitoUnreal_TPSGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACompitoUnreal_TPSGameModeBase, 1756299637);
	template<> COMPITOUNREAL_TPS_API UClass* StaticClass<ACompitoUnreal_TPSGameModeBase>()
	{
		return ACompitoUnreal_TPSGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACompitoUnreal_TPSGameModeBase(Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase, &ACompitoUnreal_TPSGameModeBase::StaticClass, TEXT("/Script/CompitoUnreal_TPS"), TEXT("ACompitoUnreal_TPSGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACompitoUnreal_TPSGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

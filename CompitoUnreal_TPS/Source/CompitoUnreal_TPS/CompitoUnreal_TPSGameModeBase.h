// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CompitoUnreal_TPSGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class COMPITOUNREAL_TPS_API ACompitoUnreal_TPSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual void PawnKilled(APawn* Pawnkilled);
	
};

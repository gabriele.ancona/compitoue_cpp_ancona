// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPITOUNREAL_TPS_CompitoUnreal_TPSGameModeBase_generated_h
#error "CompitoUnreal_TPSGameModeBase.generated.h already included, missing '#pragma once' in CompitoUnreal_TPSGameModeBase.h"
#endif
#define COMPITOUNREAL_TPS_CompitoUnreal_TPSGameModeBase_generated_h

#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_SPARSE_DATA
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_RPC_WRAPPERS
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACompitoUnreal_TPSGameModeBase(); \
	friend struct Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACompitoUnreal_TPSGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CompitoUnreal_TPS"), NO_API) \
	DECLARE_SERIALIZER(ACompitoUnreal_TPSGameModeBase)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACompitoUnreal_TPSGameModeBase(); \
	friend struct Z_Construct_UClass_ACompitoUnreal_TPSGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACompitoUnreal_TPSGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CompitoUnreal_TPS"), NO_API) \
	DECLARE_SERIALIZER(ACompitoUnreal_TPSGameModeBase)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACompitoUnreal_TPSGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACompitoUnreal_TPSGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACompitoUnreal_TPSGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACompitoUnreal_TPSGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACompitoUnreal_TPSGameModeBase(ACompitoUnreal_TPSGameModeBase&&); \
	NO_API ACompitoUnreal_TPSGameModeBase(const ACompitoUnreal_TPSGameModeBase&); \
public:


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACompitoUnreal_TPSGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACompitoUnreal_TPSGameModeBase(ACompitoUnreal_TPSGameModeBase&&); \
	NO_API ACompitoUnreal_TPSGameModeBase(const ACompitoUnreal_TPSGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACompitoUnreal_TPSGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACompitoUnreal_TPSGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACompitoUnreal_TPSGameModeBase)


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_12_PROLOG
#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_SPARSE_DATA \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_RPC_WRAPPERS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_INCLASS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_SPARSE_DATA \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPITOUNREAL_TPS_API UClass* StaticClass<class ACompitoUnreal_TPSGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CompitoUnreal_TPS_Source_CompitoUnreal_TPS_CompitoUnreal_TPSGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
